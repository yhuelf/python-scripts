#!/usr/bin/python
# -*- coding: utf-8 -*-

###############################################################################

# lift_fragments_out_of_mp4.py: Lift all fragments out of an mp4 file.
# Copyright (C) 2011 Viotech Communications
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

###############################################################################

"""
This program lift all fragments out of an mp4 file, and save them
in separated files.

Documents:
- ISO/IEC 14496-12 (ISO Base Media file format)
http://standards.iso.org/ittf/PubliclyAvailableStandards/index.html

Author : Frédéric Yhuel <fyhuel _AT_ viotech _DOT_ net>
Creation : 2011-19-20
"""

import sys
from base64 import b16encode

def main():
    mp4_path = sys.argv[1]
    mp4_file = open(mp4_path)

    mp4_file.seek(-4, 2)
    mfra_length = mp4_file.read()
    mfra_length = conv(mfra_length)

    mp4_file.seek(mfra_length*(-1), 2)
    mfra = mp4_file.read()

    assert(len(mfra) == mfra_length == conv(mfra[0:4]))

    tfra_offset = -8
    while True:

        tfra_offset = mfra.find("tfra", tfra_offset+8) - 4
        if tfra_offset < 0:
            break
        tfra_length = conv(mfra[tfra_offset:tfra_offset + 4])
        tfra = mfra[tfra_offset:tfra_offset + tfra_length]

        track_id = conv(tfra[12:16])
        print("track_id is %i" % track_id)
        assert(0 < track_id < 10)
        number_of_entry = conv(tfra[20:24])
        print("There are %i entries" % number_of_entry)

        lengths = conv(tfra[19])
        length_size_of_traf_num = lengths >> 4
        length_size_of_trun_num = (lengths & 0x0c) >> 2
        length_size_of_sample_num = lengths & 0x03

        period = 16 + length_size_of_traf_num + 1
        period += length_size_of_trun_num + 1
        period += length_size_of_sample_num + 1

        print("Period is %i" % period)

        for entry_num in range(number_of_entry):
            moof_offset = conv(tfra[32+period*entry_num : 40+period*entry_num])
            mp4_file.seek(moof_offset)
            moof_length = conv(mp4_file.read(4))
            mp4_file.seek(moof_offset)
            moof = mp4_file.read(moof_length)
            mdat_length = conv(mp4_file.read(4))
            mp4_file.seek(-4, 1)
            mdat = mp4_file.read(mdat_length)

            fragment = moof + mdat
            fpath = "frag_%i_%i" % (track_id, entry_num)
            frag = open(fpath, "w")
            frag.write(fragment)
            frag.close()

    mp4_file.close()

def conv(bits):
    ret = int(b16encode(bits), 16)
    return ret


if __name__ == "__main__":
    main()

