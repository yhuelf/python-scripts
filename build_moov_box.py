#!/usr/bin/python
# -*- coding: utf-8 -*-

###############################################################################

# build_moov_box.py: Smooth Streaming manifest parser and moov box builder.
# Copyright (C) 2011 Viotech Communications
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA

###############################################################################

"""
This program parse a Smooth Streaming manifest and build the initialization
segment (ftyp + moov) required by libav to start playback of an mp4 file.

Documents:
- ISO/IEC 14496-12 (ISO Base Media file format)
http://standards.iso.org/ittf/PubliclyAvailableStandards/index.html
- ISO/IEC 14496-15 (AVC file format)
- PIFF (Protected Interoperable File Format)
http://go.microsoft.com/?linkid=9682897

Author : Frédéric Yhuel <fyhuel _AT_ viotech _DOT_ net>
Creation : 2011-09-01
"""

# Structure of moov box in a fMP4 file:

# moov
#     mvhd
#     trak
#         tkhd
#         tref
#         mdia
#             mdhd
#             hdlr
#             minf
#                 vmhd
#                 smhd
#                 hmhd
#                 dinf
#                     dref
#                 stbl
#                     stts
#                     ctts
#                     stsc
#                     stco
#                     stsz
#                     stsd
#     mvex
#         mehd
#         trex

import sys
from struct import pack
from time import time
from base64 import b16decode, b16encode
from xml.dom.minidom import parse
from pprint import pprint
from subprocess import call

# Some global variables #########################################

# num of seconds since 1904-01-01
creation_time = ((1970-1904)*365 + 17) * 24*3600 + int(time())
modif_time = creation_time

track_ID = {
        "audio": 0,
        "video": 0,
        "text": 0,
        }
next_track_ID = 1
#################################################################

def main():
    fpath = sys.argv[1]
    cqlevel = int(sys.argv[2])

    if fpath[:4] == "http":
        call(["wget", fpath, "-O", "/tmp/smooth_manifest"])
        fpath = "/tmp/smooth_manifest"

    manifest = open(fpath)
    output_path = "/tmp/piff_initialization_segment"
    output = open(output_path, "w")

    params = parse_manifest(manifest, cqlevel)

    ftyp_size = 24
    ftyp = pack(">L4s4sL4s4s", ftyp_size, "ftyp", "isml", 1, "piff", "iso2")

    mvhd = build_mvhd(params)

    atrak = build_trak("audio", params)
    mtrak = build_trak("video", params)

    mvex = build_mvex(params)

    moov_size = 8 + len(mvhd) + len(atrak) + len(mtrak) + len(mvex)
    moov_header = pack(">L4s", moov_size, "moov")

    moov = moov_header + mvhd
    moov += atrak
    moov += mtrak
    moov += mvex
    init = ftyp + moov
    output.write(init)

    manifest.close()
    output.close()

def parse_manifest(manifest, cqlevel):
    res = {}
    dom = parse(manifest)
    ss_media = dom.getElementsByTagName("SmoothStreamingMedia")[0]
    duration = ss_media.attributes["Duration"].value
    res["duration"] = int(duration)
    vstream = None
    astream = None
    streams = ss_media.getElementsByTagName("StreamIndex")
    global next_track_ID
    stream_nb = len(streams)
    for stream in streams:
        if stream.attributes["Type"].value == "video":
            vstream = stream
            if track_ID["video"] == 0:
                track_ID["video"] = 3 - next_track_ID
                next_track_ID += 1
        if stream.attributes["Type"].value == "audio":
            astream = stream
            if track_ID["audio"] == 0:
                track_ID["audio"] = 3 - next_track_ID
                next_track_ID += 1
        #if stream.attributes["Type"].value == "text":
            #tstream = stream


    try:
        DisplayWidth = vstream.attributes["DisplayWidth"].value
    except KeyError:
        DisplayWidth = 1280
    res["DisplayWidth"] = int(DisplayWidth)
    try:
        DisplayHeight = vstream.attributes["DisplayHeight"].value
    except KeyError:
        DisplayHeight = 720
    res["DisplayHeight"] = int(DisplayHeight)
    if cqlevel > 0:
        res["QualityLevels"] = 1
    else:
        try:
            QualityLevels = vstream.attributes["QualityLevels"].value
        except KeyError:
            QualityLevels = 1
        res["QualityLevels"] = int(QualityLevels)

    # VIDEO ###################################################################
    qlevels = vstream.getElementsByTagName("QualityLevel")
    mark = "00000001"
    count = 0
    for qlevel in qlevels:
        try:
            Index = int(qlevel.attributes["Index"].value)
        except KeyError, e:
            Index = count
            count += 1
        Bitrate = int(qlevel.attributes["Bitrate"].value)
        FourCC = qlevel.attributes["FourCC"].value
        try:
            MaxWidth = qlevel.attributes["MaxWidth"].value
            MaxHeight = qlevel.attributes["MaxHeight"].value
        except KeyError:
            MaxWidth = qlevel.attributes["Width"].value
            MaxHeight = qlevel.attributes["Height"].value
        res["vcodec"] = FourCC
        if cqlevel > 0:
            if Bitrate == cqlevel:
                Index = 0
            else:
                continue
        CodecPrivateData = qlevel.attributes["CodecPrivateData"].value

        if FourCC == "H264":
            assert(CodecPrivateData[0:8] == mark)
            CodecPrivateData = CodecPrivateData[8:]
            partition = CodecPrivateData.partition(mark)
            sequenceParameterSetNALUnit = partition[0]
            pictureParameterSetNALUnit = partition[2]
            res[("sequenceParameterSetNALUnit", Index)] = b16decode(sequenceParameterSetNALUnit)
            res[("pictureParameterSetNALUnit", Index)] = b16decode(pictureParameterSetNALUnit)

        elif FourCC == "WVC1":
            res[("VideoInfoHeader", 0)] = b16decode(CodecPrivateData)

        if cqlevel > 0 and Bitrate == cqlevel:
            break
    res["MaxWidth"] = int(MaxWidth)
    res["MaxHeight"] = int(MaxHeight)
    ###########################################################################

    # AUDIO
    qlevels = astream.getElementsByTagName("QualityLevel")
    for qlevel in qlevels:
        try:
            Index = int(qlevel.attributes["Index"].value)
        except KeyError, e:
            Index = 0
        Bitrate = int(qlevel.attributes["Bitrate"].value)
        try:
            FourCC = qlevel.attributes["FourCC"].value
        except KeyError:
            FourCC = "WMAP"
        res["acodec"] = FourCC
        try:
            CodecPrivateData = qlevel.attributes["CodecPrivateData"].value
        except KeyError:
            CodecPrivateData = qlevel.attributes["WaveFormatEx"].value
        try:
            SamplingRate = qlevel.attributes["SamplingRate"].value
        except KeyError:
            SamplingRate = 44100
        res[("SamplingRate", Index)] = int(SamplingRate)
        try:
            Bitrate = qlevel.attributes["Bitrate"].value
        except KeyError:
            Bitrate = 96000
        res[("Bitrate", Index)] = int(Bitrate)


        if FourCC == "WMAP":
            res[("WaveFormatEx", Index)] = b16decode(CodecPrivateData)

        elif FourCC == "AACL":
            res[("AACL", Index)] = b16decode(CodecPrivateData)


    #pprint(res)
    return res


def build_trak(track_type, params):
    # PIFF : The width and height for a non-visual track MUST be 0

    duration = params["duration"]
    if track_type == "audio":
        volume = 0x0100
    else:
        volume = 0
    if track_type == "video":
        #width  = 0x00e00000 #FIXME
        width  = params["MaxWidth"]
        width <<= 16
        #height = 0x00800000 #FIXME
        height = params["MaxHeight"]
        height <<= 16
    else:
        width  = 0
        height = 0
    matrix = (0x00010000,0,0,0,0x00010000,0,0,0,0x40000000)
    tkhd_size = 0x68
    tkhd = pack(">L4sL", tkhd_size, "tkhd", 0x01000007)
    tkhd += pack(">QQLLQ", creation_time, modif_time, track_ID[track_type], 0, duration)
    tkhd += pack(">QhhhH9l", 0, 0, 0, volume, 0, *matrix)
    tkhd += pack(">LL", width, height)

    # PIFF : This box SHOULD appear only for video tracks that have
    # a corresponding chapter track
    tref = "" #FIXME

    minf = build_minf(track_type, params)

    timescale = 0x00989680 #FIXME
    mdhd = pack(">L4sL", 0x2c, "mdhd", 0x01000000)
    mdhd += pack(">QQLQ", creation_time, modif_time, timescale, duration)
    mdhd += pack(">HH", 0x55c4, 0)

    hdlr_size = 0x26
    hdlr = pack(">L4sL", hdlr_size, "hdlr", 0)
    # PIFF : Handler_type value of "hint" SHOULD NOT be used.
    # If it is included it MAY be ignored
    if track_type == "video":
        name = "Video"
        handler_type = "vide"
    elif track_type == "audio":
        name = "Audio"
        handler_type = "soun"
    else:
        name = "Undfd"
        handler_type = "ndfd"
    hdlr = hdlr + pack(">L4s3L6s", 0, handler_type, 0, 0, 0, name)

    mdia_size = 8 + len(mdhd) + len(hdlr) + len(minf)
    mdia_header = pack(">L4s", mdia_size, "mdia")

    trak_size = 8 + tkhd_size + len(tref)  + mdia_size
    trak_header = pack(">L4s", trak_size, "trak")

    mdia = mdia_header + mdhd + hdlr + minf
    trak = trak_header + tkhd + tref + mdia

    return trak

def build_minf(track_type, params):

    vmhd = pack(">L4sLQ", 20, "vmhd", 0x00000001, 0)
    smhd = pack(">L4sLhH", 16, "smhd", 0, 0, 0)
    nmhd = pack(">L4sL", 20, "nmhd", 0)

    if track_type == "video":
        xmhd = vmhd
    elif track_type == "audio":
        xmhd = smhd
    elif track_type == "metad":
        xmhd = nmhd

    dinf_size = 36
    dinf = pack(">L4sL4sL", dinf_size, "dinf", 28, "dref", 0)
    dinf = dinf + pack(">LL4sL", 1, 12, "url\x20", 0x00000001)

    stbl = build_stbl(track_type, params)

    minf_size = 8 + len(xmhd) + dinf_size + len(stbl)
    minf_header = pack(">L4s", minf_size, "minf")
    minf = minf_header + xmhd + dinf + stbl

    return minf

def build_stbl(track_type, params):

    # PIFF : The Decoding Time to Sample SHOULD contain no entries.
    stts = pack(">L4sLL", 16, "stts", 0, 0)
    # PIFF : The Composite Time to Sample SHOULD contain no entries.
    ctts = pack(">L4sLL", 16, "ctts", 0, 0)

    stsc = pack(">L4sLL", 16, "stsc", 0, 0)
    stco = pack(">L4sLL", 16, "stco", 0, 0)

    stsz = pack(">L4sLLL", 20, "stsz", 0, 0, 0)

    if track_type == "video":
        if params["vcodec"] == "H264":
            video_fmt = "avc1"
            video_extra = build_avcC(params)
            #video_extra = ""
        elif params["vcodec"] == "WVC1":
            video_fmt = "vc-1"
            video_extra = build_dvc1(params)
            #video_extra = ""

        sample_entry_size = 86 + len(video_extra)
        width = params["MaxWidth"]
        height = params["MaxHeight"]
        horiz_resolution = 0x00480000
        vert_resolution = 0x00480000
        compressor_name = "\x00"*32
        depth = 0x0018

        SampleEntry = pack(">L4sLHH", sample_entry_size, video_fmt, 0, 0, 1)
        SampleEntry += pack(">QQHH", 0, 0, width, height)
        SampleEntry += pack(">LL", horiz_resolution, vert_resolution)
        SampleEntry += pack(">LH32sHh", 0, 1, compressor_name, depth, -1)
        SampleEntry += video_extra

    elif track_type == "audio":
        if params["acodec"] == "AACL":
            audio_fmt = "mp4a"
            audio_extra = build_esds(params)
            #audio_extra = ""

        elif params["acodec"] == "WMAP":
            audio_fmt = "wma "
            audio_extra = build_wfex(params)
            #audio_extra = ""

        sample_entry_size = 36 + len(audio_extra)
        sample_rate = params[("SamplingRate", 0)]<<16
        SampleEntry = pack(">L4sLHH", sample_entry_size, audio_fmt, 0, 0, 1)
        SampleEntry += pack(">QHHLL", 0, 2, 16, 0, sample_rate)
        SampleEntry += audio_extra

    stsd_size = 16 + len(SampleEntry)
    stsd = pack(">L4sLL", stsd_size, "stsd", 0, 1)
    stsd += SampleEntry

    stbl_size = 8 + len(stts)*4 + len(stsz) + stsd_size
    stbl_header = pack(">L4s", stbl_size, "stbl")
    stbl = stbl_header + stts + ctts + stsc + stco + stsz + stsd
    return stbl

def build_esds(params):

    CodecPrivateData = params[("AACL", 0)]
    tag = 3
    length = 0x80808000 + len(CodecPrivateData) + 18 + 8
    ES_ID = 0
    priority = 0

    ES_Descriptor_header = pack(">BLHB", tag, length, ES_ID, priority)

    tag = 4
    length = 0x80808000 + len(CodecPrivateData) + 18
    obj_type_id = 0x40
    stype = 0x15
    buf = (0x00, 0x30, 0x00)
    max_bitrate = 0x0001f400
    avg_bitrate = 0x0001f400

    dec_config_descr_header = pack(">BLBB3B", tag, length, obj_type_id, stype, *buf)
    dec_config_descr_header += pack(">LL", max_bitrate, avg_bitrate)

    tag = 5
    length = 0x80808000 + len(CodecPrivateData)

    dec_specific_info_header = pack(">BL", tag, length)

    end = pack(">3H", 0x0680, 0x8080, 0x0102)

    esds_size = 12 + len(ES_Descriptor_header) + len(dec_config_descr_header) +\
            len (dec_specific_info_header) + len(CodecPrivateData) + len(end)

    esds_header = pack(">L4sL", esds_size, "esds", 0)

    esds = esds_header + ES_Descriptor_header + dec_config_descr_header +\
            dec_specific_info_header + CodecPrivateData + end

    return esds

def build_wfex(params):
    sample_rate = params[("SamplingRate", 0)]
    bitrate = params[("Bitrate", 0)]
    bitrate /= 8
    const = pack(">L", 0x62010200)
    # byte order is not as usual there
    const += pack("2L", sample_rate, bitrate)
    const += pack(">LH", 0x6b111000, 0x1200)
    WaveFormatEx = params[("WaveFormatEx", 0)]
    wfex_size = 8 + len(const) + len(WaveFormatEx)
    wfex_header = pack(">L4s", wfex_size, "wfex")
    wfex = wfex_header + const + WaveFormatEx

    return wfex

def build_dvc1(params):
    const = pack(">L3B", 0xc8802418, 0x18, 0xbd, 0x19)
    VideoInfoHeader = params[("VideoInfoHeader", 0)]
    dvc1_size = 8 + len(const) + len(VideoInfoHeader)
    dvc1_header = pack(">L4s", dvc1_size, "dvc1")
    dvc1 = dvc1_header + const + VideoInfoHeader

    return dvc1

def build_avcC(params):
    AVCProfileIndication = 0x64 #FIXME
    profile_compatibility = 0x40 #FIXME
    AVCLevelIndication = 0x1f #FIXME
    lengthSizeMinusOne = 0x03 #FIXME

    numOfSequenceParameterSets = params["QualityLevels"]

    AVCDecoderConfigurationRecord = pack(">BB", 1, AVCProfileIndication)
    AVCDecoderConfigurationRecord += pack(">B", profile_compatibility)
    AVCDecoderConfigurationRecord += pack(">B", AVCLevelIndication)
    AVCDecoderConfigurationRecord += pack(">B", 0xfc + lengthSizeMinusOne)
    AVCDecoderConfigurationRecord += pack(">B", 0xe0 + numOfSequenceParameterSets)

    for i in range(numOfSequenceParameterSets):
        sequenceParameterSetNALUnit = params[("sequenceParameterSetNALUnit", i)]
        sequenceParameterSetLength = len(sequenceParameterSetNALUnit)
        AVCDecoderConfigurationRecord += pack(">H", sequenceParameterSetLength)
        fmt = ">%is" % sequenceParameterSetLength
        AVCDecoderConfigurationRecord += pack(fmt, sequenceParameterSetNALUnit)

    #numOfPictureParameterSets = params["QualityLevels"]
    numOfPictureParameterSets = 1
    AVCDecoderConfigurationRecord += pack(">B", numOfPictureParameterSets)

    for i in range(numOfPictureParameterSets):
        pictureParameterSetNALUnit = params[("pictureParameterSetNALUnit", i)]
        pictureParameterSetLength = len(pictureParameterSetNALUnit)
        AVCDecoderConfigurationRecord += pack(">H", pictureParameterSetLength)
        fmt = ">%is" % pictureParameterSetLength
        AVCDecoderConfigurationRecord += pack(fmt, pictureParameterSetNALUnit)

    avcC_size = 8 + len(AVCDecoderConfigurationRecord)
    avcC_header = pack(">L4s", avcC_size, "avcC")
    avcC = avcC_header + AVCDecoderConfigurationRecord

    return avcC

def build_mvex(params):
    mvex_size = 92
    mvex_header = pack(">L4s", mvex_size, "mvex")

    fragment_duration = params["duration"]
    mehd = pack(">L4sLQ", 20, "mehd", 0x01000000, fragment_duration)

    vtrex = pack(">L4sLLLLLL", 32, "trex", 0, 1, 1, 0, 0, 0)
    atrex = pack(">L4sLLLLLL", 32, "trex", 0, 2, 1, 0, 0, 0)
    mvex = mvex_header + mehd + vtrex + atrex

    return mvex

def build_mvhd(params):
    mvhd_size = 120
    timescale = 10000000 # 100 ns
    duration = params["duration"]

    # PIFF : all five param must have their default value
    rate = 0x00010000
    volume = 0x0100
    reserved = 0
    lreserved = 0
    matrix = (0x00010000,0,0,0,0x00010000,0,0,0,0x40000000)

    pre_defined = (0,0,0,0,0,0)

    mvhd = pack(">L4sL", mvhd_size, "mvhd", 0x01000000)
    mvhd += pack(">QQLQ", creation_time, modif_time, timescale, duration)
    mvhd += pack(">LHHQ9L", rate, volume, reserved, lreserved, *matrix)
    mvhd += pack(">6L", *pre_defined)
    mvhd += pack(">L", next_track_ID)

    return mvhd


if __name__ == "__main__":
    main()
    pprint(track_ID)
    print "next_track_ID is %i" % next_track_ID
