#!/usr/bin/python
# -*- coding: utf-8 -*-

import shutil
import sys
from time import time
from xml.dom.minidom import parse
from subprocess import call


def main():

    fpath = sys.argv[1]
    stream_name = sys.argv[2]
    manifest = open(fpath)


    dom = parse(manifest)
    ss_media = dom.getElementsByTagName("SmoothStreamingMedia")[0]
    streams = ss_media.getElementsByTagName("StreamIndex")
    mstream = None
    for stream in streams:
        if stream.attributes["Name"].value == stream_name:
            mstream = stream

    durations = mstream.getElementsByTagName("c")

    count = 0
    st = 0
    for duration in durations:
        dur = int(duration.attributes["d"].value)
        src = "../ed_2962/frag_2_%i" % count
        dst = "Fragments(%s=%i)" % (stream_name, st)
        #print src
        #print dst
        shutil.copy(src, dst)
        st += dur
        count += 1

    manifest.close()

    return 0

if __name__ == "__main__":
    main()
