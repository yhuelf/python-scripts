#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import sys
from pprint import pprint

def main():
    csv_path = sys.argv[1]
    output = sys.argv[2]

    video_list = []
    min_dssim = 29
    max_dssim = 76
    ratio = int(255.0 / (max_dssim - min_dssim))

    with open(csv_path, 'rb') as fin:
        with open(output, 'a') as fout:
            mind = 2**15
            maxd = 0
            reader = csv.reader(fin, delimiter=':')
            count = 0
            for row in reader:
                if count == 0:
                    for cell in row:
                        if cell not in ("", " "):
                            video_list.append(cell)
                    count += 1
                elif 0 < count < 3:
                    count += 1
                    continue
                elif count >= 3:
                    for k, VID in enumerate(video_list):
                        for i, FTY in enumerate(("I", "P", "B")):
                            for j, cell in enumerate(row):
                                if j == 0:
                                    fout.write("ARC=\"%s\", " % cell)
                                if j == 1:
                                    fout.write("MCE=\"%s\", " % cell)
                                if j == 2:
                                    fout.write("AVG=\"%s\", " % cell)
                                if j == 5 + i*4 + 12*k:
                                    dssim = int(1.0/(1-float(cell)))
                                    if dssim < mind:
                                        mind = dssim
                                    if dssim > maxd:
                                        maxd = dssim
                                    sdssim = (dssim - min_dssim) * ratio
                                    fout.write("SSIM=\"%i\", " % sdssim)
                                    #fout.write("SSIM=\"%s\", " % str(float(i+k+j)/400))
                            fout.write("FTY=\"%s\", " % FTY)
                            fout.write("VID=\"%s\";\n" % VID)
            fout.write("}")
    pprint(video_list)
    print("maxd is %i" % maxd)
    print("mind is %i" % mind)



if __name__ == "__main__":
    main()
